# hdevice

#### 介绍
判断系统，系统版本，浏览器类型(chrome,uc,ie,edge,safari,opera),浏览器版本，是否在微信打开，设备(window,android,iphone,ipad),设备型号(iphone4,iphone5,iphone7,iphonex....,)

#### 暴露字段

hdevice

#### 使用说明

1. <script src="https://hcsy.gitee.io/hdevice/dist/hdevice.js"></script>

#### 接口说明
1. getResult()<br>
  获取检测结果<br>
  使用： hdevice.getResult()<br>
  类似<br>
  {
     browser: "chrome" //浏览器类型<br>
     browser_version: "74.0.3729.131" //浏览器版本<br>
     deviceMemory: 8 //设备内存 目前支持的很少<br>
     device_type: "iphone7" //设备型号<br>
     is_weixn: false //是否在微信打开<br>
     system: "window" //系统<br>
     system_version: "win10" //系统型号<br>
  }
  <br>
2. getResultAsync()<br>
  同上 是个异步函数 使用 then<br>
  其余方法均是同步方式<br>
3. is_weixn()<br>
  是否在微信打开<br>
4. detectOS()<br>
  获取系统<br>
5. getExplore()<br>
  获取浏览器信息<br>
6. getOSVersion()<br>
   获取系统版本<br>
